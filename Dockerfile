FROM python:3.7-alpine

WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

EXPOSE 5000

COPY ./src .

CMD ["flask", "run"]

#
#
#
#FROM ubuntu:20.04
#
#MAINTAINER Guilhelm PANAGET "guilhelm@gpyd.pw"
#
#RUN apt-get update -y && \
#    apt-get install -y python3-pip
#
#COPY ./requirements.txt /app/requirements.txt
#
#WORKDIR /app
#
#RUN pip install -r requirements.txt
#
#COPY ./src /app
#
#EXPOSE 8050
#
#ENTRYPOINT [ "python3" ]
#
#CMD [ "app.py" ]
